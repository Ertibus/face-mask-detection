Face Mask Detection
==================================
Table of contents
-----------------
.. contents::


About
------------
This Python program detects whether the person has a mask on.
It does this by first collecting face data and then using that data with the help of Scikit-learn library to predict whether the detected face has a mask.



Dependencies
------------
* joblib==1.1.0
* numpy==1.21.4
* opencv-python==4.5.4.60
* scikit-learn==1.0.1
* scipy==1.7.3
* sklearn==0.0
* threadpoolctl==3.0.0
