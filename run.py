#!/usr/bin/env python3
import cv2
import numpy as np
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

WITH_MASK_PATH: str = "yes_mask.npy"
WITHOUT_MASK_PATH: str = "no_mask.npy"
SAMPLE_COUNT: int = 200
TEST_SIZE: float = 0.25
SCALE_FACTOR: float = 1.1
MIN_NEIGHBORS: int = 4

def collect_data(save_destination: str):
    capture = cv2.VideoCapture(0)
    data = []
    while True:
        flag, img = capture.read()
        if not flag: continue

        face_cascades = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascades.detectMultiScale(gray, SCALE_FACTOR, MIN_NEIGHBORS)

        if len(faces) > 2: continue

        for x, y,w, h in faces:
            cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 4)
            face = img[y:y+h, x:x+w, :]
            face = cv2.resize(face, (50, 50))

            print('Capturing face data: ', round((len(data) + 1) / SAMPLE_COUNT * 100, 2), "%")
            if len(data) < SAMPLE_COUNT:
                data.append(face)

        cv2.imshow('Result', img)
        if cv2.waitKey(2) == 27 or len(data) >= SAMPLE_COUNT:
            break

    np.save(save_destination, data)

    cv2.destroyAllWindows()
    capture.release()

def detect_masks():
    # MACHINE-LEARNING
    data_with_mask = np.load(WITH_MASK_PATH)
    data_without_mask = np.load(WITHOUT_MASK_PATH)

    data_with_mask = data_with_mask.reshape(SAMPLE_COUNT, 50 * 50 * 3)
    data_without_mask = data_without_mask.reshape(SAMPLE_COUNT, 50 * 50 * 3)

    training_data = np.r_[data_with_mask, data_without_mask]
    data_labels = np.zeros(training_data.shape[0])
    data_labels[SAMPLE_COUNT: ] = 1.0

    x_train, _, _, _ = train_test_split(training_data, data_labels, test_size=TEST_SIZE)
    _, x_test, y_train, y_test = train_test_split(training_data, data_labels, test_size=TEST_SIZE)

    svc = SVC()
    svc.fit(x_train, y_train)

    y_pred = svc.predict(x_test)

    print("Tested accuracy => ", accuracy_score(y_test, y_pred))

    # DETECTION
    capture = cv2.VideoCapture(0)
    data = []
    while True:
        flag, img = capture.read()
        if not flag: continue

        face_cascades = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascades.detectMultiScale(gray, SCALE_FACTOR, MIN_NEIGHBORS)

        if len(faces) > 2: continue

        for x, y,w, h in faces:
            face = img[y:y+h, x:x+w, :]
            face = cv2.resize(face, (50, 50))
            face = face.reshape(1, -1)
            pred = svc.predict(face)[0]

            if int(pred) == 0:
                cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 4)
            if int(pred) == 1:
                cv2.rectangle(img, (x, y), (x+w, y+h), (0, 0, 255), 4)

        cv2.imshow('Result', img)
        if cv2.waitKey(2) == 27:
            break

    cv2.destroyAllWindows()
    capture.release()

if __name__ == "__main__":
    while True:
        print("Operations:",
            "Full data collection [1 / full]",
            "Face mask detection [2 / detect / run]",
            "Masked data collection [3 / mask]",
            "Without mask data collection [4 / nomask]",
            "Exit [0 / exit]",
            sep = '\n')

        inp = input("What to do => ")

        if inp == 'full' or inp == '1':
            input('Press enter to collect face data (NO MASK)')
            collect_data(WITHOUT_MASK_PATH)
            input('Press enter to collect face data (MASK)')
            collect_data(WITH_MASK_PATH)
        elif inp == 'detect' or inp == '2' or inp == 'run':
            detect_masks()
        elif inp == 'mask' or inp == '3':
            input('Press enter to collect face data (MASK)')
            collect_data(WITH_MASK_PATH)
        elif inp == 'nomask' or inp == '4':
            input('Press enter to collect face data (NO MASK)')
            collect_data(WITHOUT_MASK_PATH)
        elif inp == 'exit' or inp == '0':
            break
